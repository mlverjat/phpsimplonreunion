<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Ajouter une randonnée</title>
	<link rel="stylesheet" href="css/basics.css" media="screen" title="no title" charset="utf-8">
</head>

<body>
	<?php
	if ($_GET) :
		$id = $_GET['id']; //Récupérer l'id transmis par read.php dans l'adresse Url ex http://localhost:8000/update.php?id=1
		echo "Ligne à modifier = " . $id;

		//CONNEXION AU SERVEUR de BDD EN PHP ORIENTE OBJET PDO //PDO GESTION DES ERREURS DE CONNEXION
		try {
			$dbh = new PDO('mysql:host=localhost;dbname=reunion_island', 'root', 'Simplon007!');
		} catch (PDOException $e) {
			print "Erreur! Petit malin tu n'as pas réussi à te connecter à ta BDD Vérifie la connexion à ton serveur!!!" . $e->getMessage() . "<br/>";
			die();
		}

		//Lecture dans la base de la ligne à modifier
		foreach ($dbh->query("SELECT*from hiking WHERE id=$id ") as $row) {
			echo $row['name'] . ' ' . $row['difficulty'] . ' ' . $row['distance'] . ' ' . $row['duration'] . ' ' . $row['height_difference'];
			$nameid = $row['name'];
			$difficultyid = $row['difficulty'];
			$distanceid = $row['distance'];
			$durationid = $row['duration'];
			$heightid = $row['height_difference'];
			echo $nameid . '</br>';
			echo $difficultyid . '</br>';
			//transmission des infos à mon formulaire
		}

		$dbh = null;
	elseif ($_POST) :
		$id = $_POST['id2'];


		?>

	<?php endif; ?>


	<?php if ($_POST) : // grace au if ($post) le php agira que lors du post (clic submit)
		//CONNEXION AU SERVEUR de BDD EN PHP ORIENTE OBJET PDO //PDO GESTION DES ERREURS DE CONNEXION
		try {
			$dbh = new PDO('mysql:host=localhost;dbname=reunion_island', 'root', 'Simplon007!');
		} catch (PDOException $e) {
			print "Erreur! Petit malin tu n'as pas réussi à te connecter à ta BDD Vérifie la connexion à ton serveur!!!" . $e->getMessage() . "<br/>";
			die();
		}
		//RECUPERATION DES DONNEES DU FORMULAIRE
		/*UPDATE client
		SET rue = '49 Rue Ameline',
		  ville = 'Saint-Eustache-la-Forêt',
		  code_postal = '76210'
		WHERE id = 2*/
		//UPDATE `hiking` SET `name` = 'Histoire du roux', `distance` = '150' WHERE `hiking`.`id` = 7
		$id = $_POST['id2'];
		$requete = "UPDATE hiking SET name = ?, difficulty = ? , distance = ?, duration = ?,height_difference = ? WHERE id=$id";

		$insertion = $dbh->prepare($requete);
		$insertion->execute(array($_POST['name'], $_POST['difficulty'], intval($_POST['distance']), $_POST['duration'], intval($_POST['height_difference'])));

		echo "La randonnée a été modifier avec succès.</br>
		D'ailleurs: vous avez rentré les informations suivantes: </br>";
		//echo $_POST['name'] . ' ' . $_POST['difficulty'] . ' ' . intval($_POST['distance']) . ' ' . $_POST['duration'] . ' ' . intval($_POST['height_difference']);

		foreach ($dbh->query("SELECT*from hiking WHERE id = $id ") as $row) {
			echo $row['name'] . ' ' . $row['difficulty'] . ' ' . $row['distance'] . ' ' . $row['duration'] . ' ' . $row['height_difference'];

			$nameid = $row['name'];
			$difficultyid = $row['difficulty'];
			$distanceid = $row['distance'];
			$durationid = $row['duration'];
			$heightid = $row['height_difference'];
			echo $nameid . '</br>';
			echo $difficultyid . '</br>';
		}



		$dbh = null;
		?>
	<?php endif; ?>

	<a href="/read.php">Liste des données</a>
	<h1>Ajouter ou modifier</h1>
	<form action="update.php" method="post">
		<div>
			<label for="id2">id</label>
			<!--<input type="int" name="id2" value="<?= $id ?>">-->
			<select name="id2">
				<option value="<?= $id ?>">"<?= $id ?>"</option>
			</select>
		</div>
		<div>
			<label for="name">Name</label>
			<input type="text" name="name" value="<?= $nameid ?>">
		</div>

		<div>
			<label for="difficulty">Difficulté</label>
			<select name="difficulty">
				<option value="très facile" <?= "$difficultyid" === 'très facile' ? 'selected' : '' ?>>Très facile</option>
				<option value="facile" <?= "$difficultyid" === 'facile' ? 'selected' : '' ?>>Facile</option>
				<option value="moyen" <?= "$difficultyid" === 'moyen' ? 'selected' : '' ?>>Moyen</option>
				<option value="difficile" <?= "$difficultyid" === 'difficile' ? 'selected' : '' ?>>Difficile</option>
				<option value="très difficile" <?= "$difficultyid" === 'très difficile' ? 'selected' : '' ?>>Très difficile</option>
			</select>
		</div>

		<div>
			<label for="distance">Distance</label>
			<input type="text" name="distance" value="<?= $distanceid ?>">
		</div>
		<div>
			<label for="duration">Durée</label>
			<input type="duration" name="duration" value="<?= $durationid ?>">
		</div>
		<div>
			<label for="height_difference">Dénivelé</label>
			<input type="text" name="height_difference" value="<?= $heightid ?>">
		</div>
		<button type=" button" name="button">Envoyer</button>

	</form>
</body>

</html>