<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Ajouter une randonnée</title>
	<link rel="stylesheet" href="css/basics.css" media="screen" title="no title" charset="utf-8">
</head>

<body>
	<?php if ($_POST) : // grace au if ($post) le php agira que lors du post
		//CONNEXION AU SERVEUR de BDD EN PHP ORIENTE OBJET PDO //PDO GESTION DES ERREURS DE CONNEXION
		try {
			$dbh = new PDO('mysql:host=localhost;dbname=reunion_island', 'root', 'Simplon007!');
		} catch (PDOException $e) {
			print "Erreur! Petit malin tu n'as pas réussi à te connecter à ta BDD Vérifie la connexion à ton serveur!!!" . $e->getMessage() . "<br/>";
			die();
		}
		//RECUPERATION DES DONNEES DU FORMULAIRE


		$requete = "INSERT INTO `hiking` (`name`, `difficulty`, `distance`, `duration`,`height_difference`) VALUES (?, ?, ?, ?, ?)";

		$insertion = $dbh->prepare($requete);
		$insertion->execute(array($_POST['name'], $_POST['difficulty'], intval($_POST['distance']), $_POST['duration'], intval($_POST['height_difference'])));
		echo "La randonnée a été ajoutée avec succès.</br>
		D'ailleurs: vous avez rentré les informations suivantes: </br>";
		echo $_POST['name'] . ' ' . $_POST['difficulty'] . ' ' . intval($_POST['distance']) . ' ' . $_POST['duration'] . ' ' . intval($_POST['height_difference']);




		$dbh = null;
		?>
	<?php endif; ?>
	<a href="/read.php">Liste des données</a>
	<h1>Ajouter</h1>
	<form action="create.php" method="post">
		<div>
			<label for="name">Name</label>
			<input type="text" name="name" value="">
		</div>

		<div>
			<label for="difficulty">Difficulté</label>
			<select name="difficulty">
				<option value="très facile">Très facile</option>
				<option value="facile">Facile</option>
				<option value="moyen">Moyen</option>
				<option value="difficile">Difficile</option>
				<option value="très difficile">Très difficile</option>
			</select>
		</div>

		<div>
			<label for="distance">Distance</label>
			<input type="text" name="distance" value="">
		</div>
		<div>
			<label for="duration">Durée</label>
			<input type="duration" name="duration" value="">
		</div>
		<div>
			<label for="height_difference">Dénivelé</label>
			<input type="text" name="height_difference" value="">
		</div>
		<button type="submit" name="button">Envoyer</button>
	</form>
	<!--Tiré de phpmyadmin après avoir rempli les champs manuellement INSERT INTO `hiking` (`id`, `name`, `difficulty`, `distance`, `duration`,
`height_difference`) VALUES (NULL, 'La montée du Piton des neiges depuis Cilaos', 
'difficile', '15.5', '08:00:00', '1730'); -->
</body>

</html>